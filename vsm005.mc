int K;

int round(int x){
    int m;
    int y;
    m = x % 10;
    if(m < K) y = x-m;
    else y = x - m + 10;
    return y;
}

int main(){
    int q;
    int a;
    K = 7;
    q = 27;
    a = round(q);
    putint(a);
    putchar('\n');
    q = q * 2;
    a = round(q);
    putint(a);
    putchar('\n');
    return 0;
}
