
/*
 * mcc                       by Nagisa Neco Ishiura    *
 *                                                     */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "lex.h"
#include "tab.h"
#include "code.h"

#define ARRAY_MAXDIMENSION 8
#define STACK_FRAME_RESERVE 3

/* mcc のデバッグ用トレース情報の選択 */
typedef enum {
  mcc_TRACE_NO,
  mcc_TRACE_LOW,
  mcc_TRACE_MID,
  mcc_TRACE_HIGH,
} mcc_trace_t;

static mcc_trace_t mcc_trace = mcc_TRACE_NO;
static int tmp_stack1523615678 = 1741;

static void arg(int argc, char **argv, char source_f[], char object_f[], mcc_trace_t *trace);
static void argerr();
static void at(char *checkpoint);
static void syntax_error(lex_t *x, char *msg);
static void semantic_error(char *msg);
static int  id_isfunc(char *id, tab_t *gt, tab_t *lt);

static void preprocess(code_t *c);
static void postprocess(code_t *c, tab_t *gt);
static void parse_program(code_t *c, lex_t *x, tab_t *gt);
static void parse_declaration_head(code_t *c, lex_t *x, itab_basetype_t *type, int *ptrlevel, char *id);
static void parse_variable_declaration_tail(code_t *c, lex_t *x, tab_t *t, itab_cls_t cls, itab_basetype_t type, int ptrlevel, char *id);
static void parse_function_declaration_tail(code_t *c, lex_t *x, tab_t *t, itab_basetype_t type, int ptrlevel, char *id);
static void parse_function_body(code_t *c, lex_t *x, tab_t *gt, tab_t *lt, int argc);
static void parse_variable_declaration(code_t *c, lex_t *x, tab_t *t, itab_cls_t cls);
static void parse_statement(code_t *c, lex_t *x, tab_t *gt, tab_t *lt);
static void parse_expression(code_t *c, lex_t *x, tab_t *gt, tab_t *lt);
static void parse_expression2(code_t *c, lex_t *x, tab_t *gt, tab_t *lt);
static void parse_expression3(code_t *c, lex_t *x, tab_t *gt, tab_t *lt);
static void parse_expression4(code_t *c, lex_t *x, tab_t *gt, tab_t *lt);
static void parse_expression5(code_t *c, lex_t *x, tab_t *gt, tab_t *lt);
static void parse_variable_reference(code_t *c, lex_t *x, tab_t *gt, tab_t *lt);
static void parse_array_index(code_t *c, lex_t *x, tab_t *gt, tab_t *lt, tab_t *t, int i);
static void parse_assign(code_t *c, lex_t *x, tab_t *gt, tab_t *lt);
static void parse_if(code_t *c, lex_t *x, tab_t *gt, tab_t *lt);
static void parse_while(code_t *c, lex_t *x, tab_t *gt, tab_t *lt);
static void parse_return(code_t *c, lex_t *x, tab_t *gt, tab_t *lt);
static void parse_call(code_t *c, lex_t *x, tab_t *gt, tab_t *lt);
static void parse_args(code_t *c, lex_t *x, tab_t *gt, tab_t *lt);  // Modified
static void parse_lhs_expression(code_t *c, lex_t *x, tab_t *gt, tab_t *lt);

static void syntax_print(lex_t *x, char *msg);

int main(int argc, char **argv)
{
  char source_f[FILENAME_MAX]; /* mini-C プログラムのファイル名 */
  char object_f[FILENAME_MAX]; /* VSM コードのファイル名 */

  /* (0) 変数 x, c, gt, lex_trace の宣言をこの下に書き込む */
  lex_t* x;
  code_t* c;
  tab_t* gt;
  lex_trace_t lex_trace;

  /* (1) 引数の解析 */
  arg(argc, argv, source_f, object_f, &mcc_trace);

  /* (2)〜(7)の処理をこの下に書き込む */
  x = lex_new(source_f);
  c = code_new();
  gt = tab_new(lex_TOKEN_MAXLEN);

  switch(mcc_trace) {
      case mcc_TRACE_NO: lex_trace = lex_TRACE_NO; break;
      case mcc_TRACE_LOW: lex_trace = lex_TRACE_BY_CHAR; break;
      case mcc_TRACE_MID: lex_trace = lex_TRACE_BY_TOKEN; break;
      case mcc_TRACE_HIGH: lex_trace = lex_TRACE_BY_TOKEN; break;
      default: assert(0); /* エラー */
  }
  lex_trace_set(x, lex_trace);
  preprocess(c);
  lex_get(x);
  parse_program(c, x, gt);
  postprocess(c, gt);

  code_write(c, object_f);

  tab_delete(gt);
  code_delete(c);
  lex_delete(x);
  return 0;
}


static void argerr()
/* 引数エラー (コマンドラインシンタックスを表示) */
{
  fprintf(stderr, "syntax: mcc [-t TRACE_LEVEL][-o file] PROG.mc\n");
  exit(EXIT_FAILURE);
}

static void arg(
  int argc,
  char **argv,
  char source_f[],
  char object_f[],
  mcc_trace_t *mcc_trace
)
/* 引数の解析 */
{
  char *suffix;
  int i = 1;
  strcpy(object_f, "");
  if (i>=argc) argerr();
  while (argv[i][0]=='-') {
    if (strcmp(argv[i],"-t")==0) {
      i++;
      if (i>=argc) argerr();
      switch(atoi(argv[i])) {
        case 0: *mcc_trace = mcc_TRACE_NO; break;
        case 1: *mcc_trace = mcc_TRACE_LOW; break;
        case 2: *mcc_trace = mcc_TRACE_MID; break;
        case 3: *mcc_trace = mcc_TRACE_HIGH; break;
        default:  *mcc_trace = mcc_TRACE_NO;
      }
      i++;
    }
    else if (strcmp(argv[i],"-o")==0) {
      i++;
      if (i>=argc) argerr();
      strcpy(object_f, argv[i]);
      i++;
    }
    else argerr();
    if (i>=argc) argerr();
  }
  if (i>=argc) argerr();
  strcpy(source_f, argv[i]);

  if (strlen(object_f)==0) {
    strcpy(object_f,source_f);
    suffix = object_f+strlen(object_f)-3;
    if (strcmp(suffix,".mc")==0) {
      strcpy(suffix,".vsm");
    }
    else {
      strcpy(object_f+strlen(object_f),".vsm");
    }
  }
}


static void at(char *checkpoint)
/* デバッグ用: チェックポイントの表示 (主に関数の入口用) */
{
  if (mcc_trace>=mcc_TRACE_MID) {
    fprintf(stderr, "at %s\n", checkpoint);
  }
}


static void end(char *checkpoint)
/* デバッグ用: チェックポイントの表示 (主に関数の出口用) */
{
  if (mcc_trace>=mcc_TRACE_MID) {
    fprintf(stderr, "end %s\n", checkpoint);
  }
}


static void syntax_error(lex_t *x, char *msg)
/* 文法エラー */
{
  fprintf(stderr, "%s:%d: %s (last token '%s')\n",
    x->filename, x->linenum, msg, x->token);
  exit(EXIT_FAILURE);
}


static void semantic_error(char *msg)
/* 意味エラー */
{
  fprintf(stderr, "%s\n", msg);
  exit(EXIT_FAILURE);
}


static void preprocess(code_t *c)
{
  at("preprocess");
  code_append(c, opcode_ISP, 0, 0);
  code_append(c, opcode_LC, 0, 0);
  code_append(c, opcode_SB, 1, 0);
  code_append(c, opcode_CALL, 0, 0);
  code_append(c, opcode_EXIT, 0, 0);
}

static void postprocess(code_t *c, tab_t *gt)
{
  int i;
  at("postprocess");
  code_set(c, 0, opcode_ISP,  gt->itab_vsize, 0);
  code_set(c, 1, opcode_LC,  gt->itab_vsize, 0);

  i = tab_itab_find(gt, "main");
  if (i == itab_FAIL) {
      semantic_error("body of ’main’ not defined");
  }
  else if (gt->itab[i].role == itab_role_VAR) {
      semantic_error("’main’ is declared as a variable");
  }
  else if (gt->itab[i].role == itab_role_FUNC) {
      code_set(c, 3, opcode_CALL,  gt->itab[i].address, 0);
  }
  else {
    assert(0);
    }
}

static void parse_program(code_t *c, lex_t *x, tab_t *gt)
{
  at("parse_program");
  itab_basetype_t type;
  int ptrlevel;
  char id[lex_TOKEN_MAXLEN+1];

  while(x->type!=token_EOF){
      parse_declaration_head(c, x, &type, &ptrlevel, id);
      if(x->type==token_LPAREN){
          parse_function_declaration_tail (c, x, gt, type, ptrlevel, id);
      }
      else{
          parse_variable_declaration_tail (c, x, gt, itab_cls_GLOBAL, type, ptrlevel, id);
          if(x->type == token_SEMICOLON){
              lex_get(x);
          }
          else{
              syntax_error(x, "';' expected");
          }
      }
  }
}


static void parse_declaration_head(code_t *c, lex_t *x, itab_basetype_t *type, int *ptrlevel, char *id)
{
  at("parse_declaration_head");
  if (x->type == token_KW_INT) { *type = itab_basetype_INT; }
  else if (x->type == token_KW_CHAR) { *type = itab_basetype_CHAR; }
  else { syntax_error(x, "type name expected "); }
  lex_get(x);
  *ptrlevel = 0;
  while (x->type == token_STAR) {
      (*ptrlevel)++;
      lex_get(x);
  }
  if (x->type == token_ID) {
      strcpy(id, x->token);
      lex_get(x);
  }
  else { syntax_error(x, "identifier expected"); }
}


static void
parse_variable_declaration_tail(code_t *c, lex_t *x, tab_t *t, itab_cls_t cls, itab_basetype_t type, int ptrlevel, char *id)
{
  int max[ARRAY_MAXDIMENSION];
  int elementsize[ARRAY_MAXDIMENSION];
  int i = 0;
  int d = 0;
  int size = 1;
  at("parse_variable_declaration_tail");

  int dimension = 0;
  while(x->type == token_LBRACK){
      assert(dimension<ARRAY_MAXDIMENSION);
      lex_get(x);
      if(x->type == token_INT){
          max[dimension] = x->val;
          dimension++;
          lex_get(x);
      }
      else{
          syntax_error(x, "integer expected");
      }

      if(x->type == token_RBRACK){
          lex_get(x);
      }
      else{
          syntax_error(x, "']' expected");
      }
  }

  if(dimension == 0){ size = 1; }
  else{
      elementsize[dimension-1] = 1;
      for (d=dimension-2; 0<=d; d--) {
          elementsize[d] = elementsize[d+1] * max[d+1];
      }
      size = max[0]*elementsize[0];
  }

  i = tab_itab_new(t, id);
  if (i == itab_FAIL) {
      char errmsg[lex_TOKEN_MAXLEN+256];
      sprintf(errmsg, "multiple definition of %s", id);
      syntax_error(x, errmsg);
  }

  t->itab[i].role = itab_role_VAR;
  t->itab[i].cls = cls;
  t->itab[i].basetype = type;
  t->itab[i].ptrlevel = ptrlevel;
  t->itab[i].argc = dimension;
  t->itab[i].size = size;
  t->itab[i].address = t->itab_vsize;
  t->itab_vsize += size;

  if (dimension==0) {
      t->itab[i].aref = -1;
  }
  else {
      for (d=0; d<dimension; d++) {
          int a = tab_atab_append(t, max[d], elementsize[d]);
          if (d==0) t->itab[i].aref = a;
      }
  }

}


static void parse_function_declaration_tail
(code_t *c, lex_t *x, tab_t *gt, itab_basetype_t type, int ptrlevel, char *id)
{
  tab_t *lt;
  int argc;
  int i;
  at("parse_function_declaration_tail");
  lt = tab_new(lex_TOKEN_MAXLEN);
  if(x->type == token_LPAREN){
      lex_get(x);
  }
  else{
      syntax_error(x, "'(' expected");
  }
  argc = 0;
  while(x->type != token_RPAREN){
      argc++;
      parse_variable_declaration(c, x, lt, itab_cls_ARG);
      if(x->type != token_RPAREN){
          if(x->type == token_COMMA){
              lex_get(x);
          }
          else{
              syntax_error(x, "',' expected");
          }
      }
  }
  lex_get(x);

  i = tab_itab_new(gt, id);
  if (i == itab_FAIL) {
      char errmsg[lex_TOKEN_MAXLEN+256];
      sprintf(errmsg, "multiple definition of %s", id);
      syntax_error(x, errmsg);
  }

  gt->itab[i].role = itab_role_FUNC;
  gt->itab[i].cls = itab_cls_GLOBAL;
  gt->itab[i].basetype = type;
  gt->itab[i].ptrlevel = ptrlevel;
  gt->itab[i].argc = argc;
  gt->itab[i].aref = -1;
  gt->itab[i].address = c->size;

  if (mcc_trace>=mcc_TRACE_HIGH) {
    fprintf(stderr, "== dump list of gt ==\n");
    tab_dump(gt);
  }

  parse_function_body(c, x, gt, lt, argc);
  gt->itab[i].size = c->size - gt->itab[i].address;
  tab_delete(lt);

}

static void parse_function_body(code_t *c, lex_t *x, tab_t *gt, tab_t *lt, int argc)
{
  at("parse_function_body");
  if (x->type==token_LBRACE) { lex_get(x); }
  else { syntax_error(x, "'{' expected"); }

  while (x->type == token_KW_INT || x->type == token_KW_CHAR) {
      parse_variable_declaration(c, x, lt, itab_cls_LOCAL);
      if (x->type == token_SEMICOLON) {
           lex_get(x);
       }
      else {
          syntax_error(x, "'int' or 'char' expected");
      }
  }
  code_append(c, opcode_ISP, lt->itab_vsize+STACK_FRAME_RESERVE, 0);

  if (mcc_trace>=mcc_TRACE_HIGH) {
    fprintf(stderr, "== dump list of lt ==\n");
    tab_dump(lt);
  }

  while (x->type != token_RBRACE) {
      parse_statement(c, x, gt, lt);
  }

  code_append(c, opcode_RET, 0, 0);

  if (x->type==token_RBRACE) { lex_get(x); }
  else { syntax_error(x, "'}' expected"); }
}

static void parse_variable_declaration(code_t *c, lex_t *x, tab_t *t, itab_cls_t cls)
{
  itab_basetype_t type;
  int ptrlevel;
  char id[lex_TOKEN_MAXLEN];
  at("parse_variable_declaration");
  parse_declaration_head(c, x, &type, &ptrlevel, id);
  parse_variable_declaration_tail(c, x, t, cls, type, ptrlevel, id);
}


static void
parse_statement(code_t *c, lex_t *x, tab_t *gt, tab_t *lt)
{
  at("parse_statement");
  if(x->type == token_ID){
      if(id_isfunc(x->token, gt, lt) == 1)
      {//関数である
          parse_call(c,x,gt,lt);
          code_append(c, opcode_ISP, -1, 0);
          if(x->type == token_SEMICOLON){
              lex_get(x);
          }else{
              syntax_error(x, "';' expected");
          }
      }else
      {//関数でない
          parse_assign(c, x, gt, lt);
      }
  }
  else if(x->type == token_KW_IF){
      parse_if(c, x, gt, lt);
  }
  else if(x->type == token_KW_WHILE){
      parse_while(c, x, gt, lt);
  }
  else if(x->type == token_SEMICOLON)
  {//読み飛ばす
      lex_get(x);
  }
  else if(x->type == token_LBRACE)
  {//ブロック
      lex_get(x);
      while(x->type != token_RBRACE){
          parse_statement(c, x, gt, lt);
      }
      lex_get(x);
  }
  else if(x->type == token_KW_RETURN){
      parse_return(c, x, gt, lt);
  }
  else if(x->type == token_STAR){
      parse_assign(c, x, gt, lt);
  }
  else{
      syntax_error(x, "'ID' expected");
  }
}

static void parse_expression(code_t *c, lex_t *x, tab_t *gt, tab_t *lt)
{
  at("parse_expression");
  parse_expression2(c, x, gt, lt);

  lex_type_t type = x->type;
  while(
      x->type == token_LT ||
      x->type == token_LE ||
      x->type == token_GT ||
      x->type == token_GE ||
      x->type == token_EQ ||
      x->type == token_NE
  ){
      lex_get(x);
      parse_expression2(c,x,gt,lt);
      if(type == token_LT){
          code_append(c, opcode_LT, 0, 0);
      }
      else if(type == token_LE){
          code_append(c, opcode_LE, 0, 0);
      }
      else if(type == token_GT){
          code_append(c, opcode_GT, 0, 0);
      }
      else if(type == token_GE){
          code_append(c, opcode_GE, 0, 0);
      }
      else if(type == token_EQ){
          code_append(c, opcode_EQ, 0, 0);
      }
      else if(type == token_NE){
          code_append(c, opcode_NE, 0, 0);
      }
      else{
          ;
      }
      type = x->type;
  }
}

static void parse_expression2(code_t *c, lex_t *x, tab_t *gt, tab_t *lt)
{
  at("parse_expression2");
  //前半部分
  lex_type_t type = x->type;
  if(type == token_PLUS || type == token_MINUS){
      lex_get(x);
  }
  parse_expression3(c,x,gt,lt);
  if(type == token_MINUS){
      code_append(c, opcode_INV, 0 , 0);
  }

  //後半部分
  type = x->type;
  while(x->type == token_PLUS || x->type == token_MINUS){
      lex_get(x);
      parse_expression3(c,x,gt,lt);
      if(type == token_PLUS){
          code_append(c, opcode_ADD, 0, 0);
      }
      else if(type == token_MINUS){
          code_append(c, opcode_SUB, 0, 0);
      }
      else{
          ;
      }
      type = x->type;
  }
}

static void parse_expression3(code_t *c, lex_t *x, tab_t *gt, tab_t *lt)
{
  at("parse_expression3");
  parse_expression4(c,x,gt,lt);
  lex_type_t type = x->type;
  while(x->type == token_STAR || x->type == token_SLASH || x->type == token_PERC){
      lex_get(x);
      parse_expression4(c,x,gt,lt);
      if(type == token_STAR){
          code_append(c, opcode_MUL, 0, 0);
      }
      else if(type == token_SLASH){
          code_append(c, opcode_DIV, 0, 0);
      }
      else if(type == token_PERC){
          code_append(c, opcode_MOD, 0, 0);
      }
      else{
          ;
      }
      type = x->type;
  }
}

static void parse_expression4(code_t *c, lex_t *x, tab_t *gt, tab_t *lt)
{
  at("parse_expression4");
  int i = 0;
  int j;
  while(x->type == token_STAR){
      i++;
      lex_get(x);
  }
  parse_expression5(c, x, gt, lt);
  for(j = 0; j < i; j++){
      code_append(c, opcode_LI, 0, 0);
  }
}

static void parse_expression5(code_t *c, lex_t *x, tab_t *gt, tab_t *lt)
{
  at("parse_expression5");
  if(x->type == token_CHAR){
      code_append(c, opcode_LC, x->val, 0);
      lex_get(x);
  }else if(x->type == token_INT){
      code_append(c, opcode_LC, x->val, 0);
      lex_get(x);
  }else if(x->type == token_LPAREN){
      lex_get(x);
      parse_expression(c, x, gt, lt);
      if(x->type == token_RPAREN){
          lex_get(x);
      }else{
          syntax_error(x, "')' expected");
      }
  }
  else if(x->type == token_ID){
      if(id_isfunc(x->token, gt, lt) == 1){
          parse_call(c, x, gt, lt);
      }else{
          parse_variable_reference(c, x, gt, lt);
      }
  }
  else if(x->type == token_AMP){
      parse_variable_reference(c, x, gt, lt);
  }
  else{
      syntax_error(x, "'int', 'char' or ')' expected");
  }
}

static void parse_variable_reference(code_t *c, lex_t *x, tab_t *gt, tab_t *lt)
{
    at("parse_variable_reference");
    int address;
    int position;
    if(x->type == token_AMP){
        /* &がある場合 */
        lex_get(x);
        position = tab_itab_find(gt, x->token);
        if(position != itab_FAIL){
            address = gt->itab[position].address;
            lex_get(x);
            if(x->type == token_LBRACK){
                code_append(c, opcode_LA, 0, address);
                parse_array_index(c, x, gt, lt, gt, position);
            }
            else{
                code_append(c, opcode_LA, 0, address);
            }
        }
        else{
            position = tab_itab_find(lt, x->token);
            if(position != itab_FAIL){
                address = lt->itab[position].address;
                lex_get(x);
                if(x->type == token_LBRACK){
                    code_append(c, opcode_LA, 1, STACK_FRAME_RESERVE + address);
                    parse_array_index(c, x, gt, lt, lt, position);
                }
                else{
                    code_append(c, opcode_LA, 1, STACK_FRAME_RESERVE + address);
                }
            }else{
                semantic_error("undefined variable");
            }
        }
    }
    else{
        /* &がない場合 */
        position = tab_itab_find(gt, x->token);
        if(position != itab_FAIL){
            address = gt->itab[position].address;
            lex_get(x);
            if(x->type == token_LBRACK){
                code_append(c, opcode_LA, 0, address);
                parse_array_index(c, x, gt, lt, gt, position);
                code_append(c, opcode_LI, 0, 0);
            }
            else{
                code_append(c, opcode_LV, 0, address);
            }
        }
        else{
            position = tab_itab_find(lt, x->token);
            if(position != itab_FAIL){
                address = lt->itab[position].address;
                lex_get(x);
                if(x->type == token_LBRACK){
                    code_append(c, opcode_LA, 1, STACK_FRAME_RESERVE + address);
                    parse_array_index(c, x, gt, lt, lt, position);
                    code_append(c, opcode_LI, 0, 0);
                }
                else{
                    code_append(c, opcode_LV, 1, STACK_FRAME_RESERVE + address);
                }
            }else{
                semantic_error("undefined variable");
            }
        }
    }
}

static void parse_array_index(code_t *c, lex_t *x, tab_t *gt, tab_t *lt, tab_t *t, int i)
{
  at("parse_array_index");
  int flag = x->type == token_LBRACK;
  int d = 1;
  while(flag){
      lex_get(x);
      parse_expression(c, x, gt, lt);
      if(x->type == token_RBRACK){
          code_append(c, opcode_LC, t->atab[t->itab[i].aref+d-1].elementsize, 0);
          code_append(c, opcode_MUL, 0, 0);
          code_append(c, opcode_ADD, 0, 0);
          lex_get(x);
          flag = x->type == token_LBRACK;
          d++;
      }else{
          syntax_error(x, "expected ']'");
      }
  }
}

static void parse_assign(code_t *c, lex_t *x, tab_t *gt, tab_t *lt)
{
  at("parse_assign");
  parse_lhs_expression(c, x, gt, lt);
  if(x->type == token_ASSIGN){
      lex_get(x);
      parse_expression(c, x, gt, lt);
      if(x->type == token_SEMICOLON)
      {
          lex_get(x);
      }else{
          syntax_error(x, "';' expected");
      }
  }else{
      syntax_error(x, "'=' expected");
  }
  code_append(c, opcode_SI, 0, 0);
}

static void parse_if(code_t *c, lex_t *x, tab_t *gt, tab_t *lt)
{
  int l1_position;
  int l2_position;
  at("parse_if");
  lex_get(x);
  if(x->type == token_LPAREN){
      lex_get(x);
      parse_expression(c, x, gt, lt);
      l1_position = code_append(c, opcode_BZ, 0, 0);
      if(x->type == token_RPAREN){
          lex_get(x);
          parse_statement(c, x, gt, lt);
          code_set(c, l1_position, opcode_BZ, c->size-l1_position - 1, 0);
          if(x->type == token_KW_ELSE){
              lex_get(x);
              l2_position = code_append(c, opcode_B, 0, 0);
              parse_statement(c, x, gt, lt);
              code_set(c, l1_position, opcode_BZ, l2_position-l1_position, 0);
              code_set(c, l2_position, opcode_B, c->size-l2_position - 1, 0);
          }
      }else{
          syntax_error(x, "')' expected");
      }
  }else
  {
    syntax_error(x, "'(' expected");
  }
}

static void parse_while(code_t *c, lex_t *x, tab_t *gt, tab_t *lt)
{
    int l1_position;
    int l2_position;
    int bz_position;
    int b_position;
  at("parse_while");
  lex_get(x);
  if(x->type == token_LPAREN){
      lex_get(x);
      l1_position = c->size;
      parse_expression(c, x, gt, lt);
      bz_position = code_append(c, opcode_BZ, 0, 0);
      if(x->type == token_RPAREN){
          lex_get(x);
          parse_statement(c, x, gt, lt);
          b_position = code_append(c, opcode_B, l1_position - c->size - 1, 0);
          l2_position = c->size;
      }
      else{
          syntax_error(x, "')' expected");
      }
      code_set(c, bz_position, opcode_BZ, b_position - bz_position, 0);
  }
  else{
      syntax_error(x, "'(' expected");
  }
}

static void parse_return(code_t *c, lex_t *x, tab_t *gt, tab_t *lt)
{
  at("parse_return");
  lex_get(x);
  code_append(c, opcode_LA, 1, 0);
  parse_expression(c, x, gt, lt);
  code_append(c, opcode_SI, 0, 0);
  code_append(c, opcode_RET, 0, 0);
  lex_get(x);
  if(x->type == token_SEMICOLON){
      lex_get(x);
  }
}

static void parse_args(code_t *c, lex_t *x, tab_t *gt, tab_t *lt){
    int flag = 1;
    int count = 0;
    while(flag){
        parse_expression(c, x, gt, lt);
        count++;

        if(x->type == token_COMMA){
            lex_get(x);
        }else if(x->type == token_RPAREN){
            flag = 0;
        }else{
            syntax_error(x, "',' or ')' expected");
        }
    }
    lex_get(x);
    code_append(c, opcode_ISP, -(STACK_FRAME_RESERVE + count), 0);
}

static void parse_call(code_t *c, lex_t *x, tab_t *gt, tab_t *lt)
{
  at("parse_call");
  if (strcmp(x->token, "putchar") == 0) {
      lex_get(x);
      if (x->type == token_LPAREN) { lex_get(x); }
      else {
          syntax_error(x, "'(' expected");
      }
      parse_expression(c,x,gt,lt);
      if (x->type == token_RPAREN) {
          lex_get(x);
      }
      else {
          syntax_error(x, "')' expected") ;
      }
      code_append(c, opcode_DUP, 0, 0);
      code_append(c, opcode_PUTC, 0, 0);
  }
  else if(strcmp(x->token, "putint") == 0){
      lex_get(x);
      if (x->type == token_LPAREN) { lex_get(x); }
      else {
          syntax_error(x, "'(' expected");
      }
      parse_expression(c,x,gt,lt);
      if (x->type == token_RPAREN) {
          lex_get(x);
      }
      else {
          syntax_error(x, "')' expected") ;
      }
      code_append(c, opcode_DUP, 0, 0);
      code_append(c, opcode_PUTI, 0, 0);
  }
  else if(strcmp(x->token, "getint") == 0){
      lex_get(x);
      if (x->type == token_LPAREN) { lex_get(x); }
      else {
          syntax_error(x, "'(' expected");
      }
      if (x->type == token_RPAREN) {
          lex_get(x);
      }
      else {
          syntax_error(x, "')' expected") ;
      }
      code_append(c, opcode_GETI, 0, 0);
  }
  else if(strcmp(x->token, "getchar") == 0){
      lex_get(x);
      if (x->type == token_LPAREN) { lex_get(x); }
      else {
          syntax_error(x, "'(' expected");
      }
      if (x->type == token_RPAREN) {
          lex_get(x);
      }
      else {
          syntax_error(x, "')' expected") ;
      }
      code_append(c, opcode_GETC, 0, 0);
  }else if(id_isfunc(x->token, gt, lt) == 1)
  {//一般関数
      code_append(c, opcode_ISP, STACK_FRAME_RESERVE, 0);
      int position = tab_itab_find(gt, x->token);
      int address = gt->itab[position].address;
      lex_get(x);
      if(x->type == token_LPAREN){
          lex_get(x);
          if(x->type == token_RPAREN){
              code_append(c, opcode_ISP, -STACK_FRAME_RESERVE, 0);
              lex_get(x);
          }
          else{
              parse_args(c, x, gt, lt);
          }

      }
      else{
          syntax_error(x, "'(' expected");
      }
      code_append(c, opcode_CALL, address, 0);
  }
  else {
      semantic_error("unknown function called");
  }
}


static void parse_lhs_expression(code_t *c, lex_t *x, tab_t *gt, tab_t *lt)
{
  at("parse_lhs_expression");
  int count = 0;
  int flag = 0;
  int address;
  while(x->type == token_STAR){
      lex_get(x);
      count++;
  }
  if(id_isfunc(x->token, gt, lt) == 0){
      int position = tab_itab_find(gt, x->token);
      if(position != itab_FAIL){
          address = gt->itab[position].address;
          code_append(c, opcode_LA, 0, address);
          lex_get(x);
          flag = x->type == token_LBRACK;
          if(flag){
              parse_array_index(c, x, gt, lt, gt, position);
          }
      }else{
          position = tab_itab_find(lt, x->token);
          if(position != itab_FAIL){
              address = lt->itab[position].address;
              code_append(c, opcode_LA, 1, STACK_FRAME_RESERVE + address);
              lex_get(x);
              flag = x->type == token_LBRACK;
              if(flag){
                  parse_array_index(c, x, gt, lt, lt, position);
              }
          }else{
              syntax_error(x, "undefined variable");
          }
      }

      int i;
      for(i = 0; i < count; i++){
          code_append(c, opcode_LI, 0, 0);
      }
  }else{
      semantic_error("variable expected");
  }
}


static int id_isfunc(char *id, tab_t *gt, tab_t *lt)
/* id が関数名なら 1 を，変数名なら 0 を返す */
{
    int isfunc = 0;
    if(tab_itab_find(lt, id) != itab_FAIL){
        isfunc = 0;
    }
    else if(
        strcmp(id, "putchar") == 0 ||
        strcmp(id, "putint") == 0 ||
        strcmp(id, "getint") == 0 ||
        strcmp(id, "getchar") == 0
    ){
        isfunc = 1;
    }
    else{
        int position = tab_itab_find(gt, id);
        if(position != itab_FAIL){
            if(gt->itab[position].role == itab_role_FUNC){
                isfunc = 1;
            }else if(gt->itab[position].role == itab_role_VAR){
                isfunc = 0;
            }else{
                semantic_error("not variable and not function");
            }
        }else{
            semantic_error("undefined id");
        }
    }
    return isfunc;
}

void syntax_print(lex_t *x, char *msg){
    fprintf(stderr, "%s:%d: %s (last token '%s')\n",
      x->filename, x->linenum, msg, x->token);
}
